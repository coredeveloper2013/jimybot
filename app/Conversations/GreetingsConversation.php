<?php

namespace App\Conversations;

use Validator;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use App\User;

class GreetingsConversation extends Conversation
{
    public function askName()
    {
        $this->ask('What is your name?', function(Answer $answer) {
            $this->bot->userStorage()->save([
                'name' => $answer->getText(),
            ]);

            $this->say('Nice to meet you'. $answer->getText());
            $this->askEmail();
        });
    }
    public function askEmail()
    {
        $this->ask('What is your email?', function(Answer $answer) {

            $validator = Validator::make(['email' => $answer->getText()], [
                'email' => 'email',
            ]);

            if ($validator->fails()) {
                return $this->repeat('That doesn\'t look like a valid email. Please enter a valid email.');
            }

            $this->bot->userStorage()->save([
                'email' => $answer->getText(),
            ]);

            $this->askMobile();
        });
    }
    public function askMobile()
    {
        $this->ask('Great. What is your mobile?', function(Answer $answer) {
            $this->bot->userStorage()->save([
                'mobile' => $answer->getText(),
            ]);

            $this->say('Great!');
            $this->regUser();
        });
    }
    public function regUser()
    {
        $question = Question::create('It looks you are a new user. Should we remember you?')
            ->callbackId('select_service')
            ->addButtons([
                Button::create('No. Proceed as Guest.')->value('No'),
                Button::create('Yes. Register me.')->value('Yes'),
            ]);

        $this->ask($question, function(Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->bot->userStorage()->save([
                    'register' => $answer->getValue(),
                ]);

                if($answer->getValue() == 'Yes'){
                    $this->enterPass();
                }elseif($answer->getValue() == 'No'){
                    $this->bot->startConversation(new SelectServiceConversation());
                }else{
                    return $this->repeat($this->regUser());
                }
            }
        });
    }
    public function enterPass(){
        $this->ask('Enter a password.', function(Answer $answer) {
            $this->bot->userStorage()->save([
                'password' => $answer->getText(),
            ]);
            $user = $this->bot->userStorage()->find();
            $user = User::create([
                'name' => $user->get('name'),
                'email' => $user->get('email'),
                'password' => bcrypt($user->get('password')),
                'phone' => $user->get('mobile'),
                'role' => 'user',
            ]);

            $this->bot->userStorage()->save([
                'id' => $user->id,
            ]);

            $this->say("Registration successful! Your Id #$user->id");
            $this->bot->startConversation(new SelectServiceConversation());
        });
    }
    public function run()
    {
        $this->askName();
    }
}
