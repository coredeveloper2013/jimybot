<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// https://christoph-rumpel.com/2018/05/how-i-built-the-laravelquiz-chatbot-with-botman-and-laravel
// https://blog.pusher.com/building-bot-using-botman-slack-telegram/


Route::get('/', function () {
    return view('welcome');
});
// Route::get('/', 'BotManController@tinker');

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
